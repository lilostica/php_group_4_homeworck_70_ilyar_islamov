<?php

namespace App\Http\Controllers\Api;

use App\Article;
use App\Http\Controllers\Controller;
use App\Http\Requests\ArticleRequest;
use App\Http\Resources\ArticleResource;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->except(['index']);
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $articles = Article::all();
        return ArticleResource::collection($articles);
    }


    /**
     * @param ArticleRequest $request
     * @return ArticleResource
     */
    public function store(ArticleRequest $request)
    {
        $article = Article::create($request->all());
        return  new ArticleResource($article);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Article  $article
     * @return ArticleResource
     */
    public function show(Article $article)
    {
        return new ArticleResource($article);
    }


    /**
     * @param ArticleRequest $request
     * @param Article $article
     * @return ArticleResource
     */
    public function update(ArticleRequest $request, Article $article)
    {
        $article->update($request->all());
        return new ArticleResource($article);
    }


    /**
     * @param Article $article
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Article $article)
    {
        $article->delete();
        return  response('',204);
    }
}
