<?php

namespace Tests\Unit;

use App\Article;
use App\Comment;
use Tests\TestCase;

class CommentTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testIndexNotAuthAsses()
    {
        $response = $this->json(
            'get',
            route('comments.index')
        );
        $response->assertStatus(200);
    }

    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testSuccessCreateComment()
    {
        $this->actingAs($this->user,'api');
        $article = factory(Article::class)
            ->create([
                'user_id' => $this->user->id
            ]);
        $data = [
            'article_id' => $article->id,
            'body' => $this->faker->text,
            'user_id' => $this->user->id
        ];
        $response = $this->json('post',route('comments.store'),$data);
        $response->assertCreated();
    }

    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testNotAuthCreateComment()
    {
        $article = factory(Article::class)
            ->create([
                'user_id' => $this->user->id
            ]);
        $data = [
            'article_id' => $article->id,
            'body' => $this->faker->text,
            'user_id' => $this->user->id
        ];
        $response = $this->json('post',route('comments.store'),$data);
        $response->assertStatus(401);
    }

    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testShowNotAuthAsses()
    {
        $comment = factory(Comment::class)->make();
        $response = $this->json(
            'get',"api/comments/{$comment}");
        $response->assertStatus(401);
    }


    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testShowAuthAsses()
    {
        $this->actingAs($this->user,'api');
        $article = factory(Article::class)
            ->create([
                'user_id' => $this->user->id
            ]);
        $comment = factory(Comment::class)
            ->create([
                'user_id' => $this->user->id,
                'article_id' => $article->id
            ]);
        $response = $this->json(
            'get',
            route('comments.show',$comment)
        );
        $response->assertStatus(200);
    }

    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testUpdateAuthAsses()
    {
        $this->actingAs($this->user,'api');
        $article = factory(Article::class)
            ->create([
                'user_id' => $this->user->id
            ]);
        $comment = factory(Comment::class)
            ->create([
                'user_id' => $this->user->id,
                'article_id' => $article->id
            ]);
        $data = [
            'article_id' => $article->id,
            'body' => $this->faker->text,
            'user_id' => $this->user->id
        ];
        $response = $this->json(
            'put',"api/comments/{$comment->id}",$data);
        $response->assertStatus(200);
    }

    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testUpdateNotAuthAsses()
    {
        $article = factory(Article::class)
            ->create([
                'user_id' => $this->user->id
            ]);
        $comment = factory(Comment::class)
            ->create([
                'user_id' => $this->user->id,
                'article_id' => $article->id
            ]);
        $data = [
            'article_id' => $article->id,
            'body' => $this->faker->text,
            'user_id' => $this->user->id
        ];
        $response = $this->json(
            'put',"api/comments/{$comment->id}",$data);
        $response->assertStatus(401);
    }

    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testDestroyNotAuthAsses()
    {
        $article = factory(Article::class)
            ->create([
                'user_id' => $this->user->id
            ]);
        $comment = factory(Comment::class)
            ->create([
                'user_id' => $this->user->id,
                'article_id' => $article->id
            ]);
        $response = $this->json(
            'delete',route('comments.destroy',$comment));
        $response->assertStatus(401);
    }

    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testDestroyAuthAsses()
    {
        $this->actingAs($this->user,'api');
        $article = factory(Article::class)
            ->create([
                'user_id' => $this->user->id
            ]);
        $comment = factory(Comment::class)
            ->create([
                'user_id' => $this->user->id,
                'article_id' => $article->id
            ]);
        $response = $this->json(
            'delete',route('comments.destroy',$comment));
        $response->assertNoContent();
    }
}
